#!/usr/bin/env python3

"""
Python 3: Web scraping using selenium
    - Download tv.txt and dv.txt files from Archivist Datasets, rename: prefix.tvlinking.txt, prefix.dv.txt
    - QV (Question Bariables) qv.txt, rename prefix.qvmapping.txt
    - TQ (Topic Questions) tq.txt, rename prefix.tqlinking.txt
"""

from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

import pandas as pd
import time
import sys
import os
import re

from mylib import get_driver, url_base, archivist_login_all, get_base_url


driver = get_driver()
n = 5

def isNaN(num):
    return num != num


def archivist_download_txt(df_base_urls, output_prefix_dir, output_type_dir, uname, pw):
    """
    Loop over prefix, downloading txt files
    """
    # Log in to all
    urls = df_base_urls["base_url"].to_list()
    print("URLs is {}".format(urls))
    ok = archivist_login_all(driver, urls, uname, pw)

    def log_to_csv(prefix, dataset_name, dataset_id):
        """append a line to spreadsheet with three values"""
        with open(os.path.join(os.path.dirname(output_prefix_dir), "dataset_list.csv"), "a") as f:
            f.write( ",".join([prefix, dataset_name, dataset_id]) + "\n")
    print(df_base_urls)
    for index, row in df_base_urls.iterrows():

        # download instrument: tqlinking, qvmapping
        # https://closer-archivist.herokuapp.com/admin/instruments/PREFIX/exports

        output_dir = os.path.join(output_prefix_dir, row["Instrument"])
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        output_tq_dir = os.path.join(output_type_dir, "tqlinking",)
        if not os.path.exists(output_tq_dir):
            os.makedirs(output_tq_dir)

        output_qv_dir = os.path.join(output_type_dir, "qvmapping",)
        if not os.path.exists(output_qv_dir):
            os.makedirs(output_qv_dir)

        output_tv_dir = os.path.join(output_type_dir, "tvlinking",)
        if not os.path.exists(output_tv_dir):
            os.makedirs(output_tv_dir)

        output_dv_dir = os.path.join(output_type_dir, "dv",)
        if not os.path.exists(output_dv_dir):
            os.makedirs(output_dv_dir)

        print(row["base_url"])
        driver.get(row["base_url"])
        time.sleep(3*n)

        driver.find_element(By.XPATH, "//button[@aria-label='open drawer']").click()
        time.sleep(n)

        AdminButton = driver.find_element(By.XPATH, "//span[text()='Admin']")
        AdminButton.click();

        ExportButton = driver.find_element(By.XPATH, "//span[text()='Instrument Exports']")
        ExportButton.click();

        print("Load export page")
        delay = 3*n # seconds
        try:
            myElem = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//input[@placeholder='Search by prefix (press return to perform search)']")))
            print("Page is ready!")
        except TimeoutException:
            print("Loading export page took too much time!")

        # find the input box
        prefix = row["Instrument"].strip()
        print(prefix)
        time.sleep(n)
        inputElement = driver.find_element(By.XPATH, "//input[@placeholder='Search by prefix (press return to perform search)']")
        inputElement.send_keys(prefix)

        # locate id and link
        #TODO: make this better
        trs = driver.find_elements(by=By.XPATH, value="html/body/div/div/div/div/main/div/div/div/div/table/tbody/tr")
        #print(trs)

        # locate which line prefix is the correct id
        keep_tr = None
        for i, tr in enumerate(trs):
            print(i)
            # column 1 is "ID"
            tr_id = tr.find_elements(by=By.XPATH, value="td")[0].text

            # column 2 is "Prefix"
            tr_prefix = tr.find_elements(by=By.XPATH, value="td")[1].text
            print(tr_prefix, tr_id)
            if (tr_prefix == prefix):
                print("found correct id")
            else:
                print("this row is for " + tr_prefix + ", not " + prefix)

        exports_page_url = row["base_url"] + "/admin/instruments/" + prefix + "/exports"
        print("Click view button for " + prefix)
        print(exports_page_url)
        driver.get(exports_page_url)
        time.sleep(n)

        try:
            myEle = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//h2[text()='TQ']")))
            print("Instrument Export Page is ready!")
        except TimeoutException:
            print("Loading Instrument Export page took too much time!")

        print("Click button " + row["Instrument"] + "/tq.txt")
        tq_href = driver.find_element(By.XPATH,  "//*[contains(@href, 'tq.txt')]")
        tq_href.click();
        print(driver.current_url)
        time.sleep(n)

        print("  Downloading " + row["Instrument"] + "/tq.txt")
        tq_content = driver.find_element(By.XPATH, "html/body").text

        if tq_content:
            with open(os.path.join(output_dir, row["Instrument"] + ".tqlinking.txt"), "w") as f:
                try:
                    f.write(tq_content.replace(" ", "\t"))
                except :
                    print("Could not download tq : ", row["Instrument"])
                    continue
            with open(os.path.join(output_tq_dir, row["Instrument"] + ".tqlinking.txt"), "w") as f:
                try:
                    f.write(tq_content.replace(" ", "\t"))
                except :
                    print("Could not download tq : ", row["Instrument"])
                    continue
            time.sleep(n)

        else:
            print("no tq contend")

        # back to the admin/instruments/*prefix*/exports page
        driver.get(exports_page_url)
        time.sleep(n)

        print("Getting " + row["Instrument"] + "/qv.txt")
        qv_href = driver.find_element(By.XPATH,  "//*[contains(@href, 'qv.txt')]")
        qv_href.click();
        print(driver.current_url)
        time.sleep(n)
        print("  Downloading " + row["Instrument"] + "/qv.txt")
        qv_content = driver.find_element(By.XPATH, "html/body").text

        if qv_content:
            with open(os.path.join(output_dir, row["Instrument"] + ".qvmapping.txt"), "w") as f:
                f.write(qv_content.replace(" ", "\t"))
            with open(os.path.join(output_qv_dir, row["Instrument"] + ".qvmapping.txt"), "w") as f:
                f.write(qv_content.replace(" ", "\t"))
            time.sleep(n)
        else:
            print("no qv content")

        driver.get(exports_page_url)
        time.sleep(n)

        # Datasets:
        # find next table after h2
        print("look for datasets")
        try:
            data_table = driver.find_element(By.XPATH, "//h2[text()='Datasets']/following-sibling::table")
            #data_table.get_attribute('innerHTML')
            trs = data_table.find_elements(By.XPATH, 'tbody/tr')
            # find list of datasets
            for i in range(len(trs)):
                instance_name = trs[i].find_element(By.XPATH, 'td').text
                data_links = [(link.text, link.get_attribute('href')) for link in trs[i].find_elements(By.TAG_NAME, 'a')]
                print(data_links)

                # loop over datasets
                if data_links == []:
                    # no dataset
                    print("no dataset found")

                elif len(data_links) > 0:
                    data_link =[y for (x,y) in data_links if x=='VIEW'][0]
                    study_id = os.path.basename(data_link.split('/export')[0])

                    log_to_csv(row["Instrument"], instance_name, data_link)

                    # contains dataset tv/dv
                    for (data_type, data_link) in [(x,y) for (x,y) in data_links if x in ('tv.txt', 'dv.txt')]:
                        print("Getting " + study_id + ', ' + data_type)
                        driver.get(data_link)
                        time.sleep(n)
                        print("  Downloading " + study_id + ', ' + data_type)
                        content = driver.find_element(By.XPATH, "html/body").text
                        print(data_link)

                        if content and content != '':
                            if data_type == 'tv.txt':
                                type_name = '.tvlinking.txt'
                                output_dir_2 = output_tv_dir
                            elif data_type == 'dv.txt':
                                type_name = '.dv.txt'
                                output_dir_2 = output_dv_dir
                            else:
                                print('    ' + study_id + ', ' + data_type + ': this should not happen, check')

                            with open(os.path.join(output_dir, instance_name + type_name), 'w') as f:
                                f.write(content.replace(' ', '\t'))
                            with open(os.path.join(output_dir_2, instance_name + type_name), 'w') as f:
                                f.write(content.replace(' ', '\t'))
                        else:
                            print('    ' + study_id + ', ' + data_type + ': no content')
                        time.sleep(n)
                        #back to the admin/instruments/*prefix*/exports page
                        driver.execute_script("window.history.go(-1)")
                        time.sleep(2*n)
                else:
                    print('data_links error')

        except NoSuchElementException:
            print("exception handled")

    driver.quit()


def get_txt(df, output_prefix_dir, output_type_dir, uname, pw):
    """
    Export txt files to output dir
    """

    df_base_urls = get_base_url(df)
    df_base_urls['base_url'] = df_base_urls['base_url'].apply(lambda x: url_base(x))

    archivist_download_txt(df_base_urls, output_prefix_dir, output_type_dir, uname, pw)


def main():
    uname = sys.argv[1]
    pw = sys.argv[2]

    main_dir = "export_txt"
    output_prefix_dir = os.path.join(main_dir, "txt_by_prefix")
    if not os.path.exists(output_prefix_dir):
        os.makedirs(output_prefix_dir)
    output_type_dir = os.path.join(main_dir, "txt_by_type")
    if not os.path.exists(output_type_dir):
        os.makedirs(output_type_dir)

    # Hayley's txt as dataframe
    df = pd.read_csv("Prefixes_to_export.txt", sep="\t")

    get_txt(df, output_prefix_dir, output_type_dir, uname, pw)


if __name__ == "__main__":
    main()

